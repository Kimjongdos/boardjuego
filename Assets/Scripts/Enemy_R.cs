﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_R : MonoBehaviour
{
    
    private int currentPoint = 0;
   
    public GameObject Bullet;
    public float TimeToShoot;
    private float Counter;   
    public Transform PlaceToShoot;
    public GameObject FXDisparo;
    
   
    void Update()
    {
        Shoot();
        
    }

   

    public void Shoot(){
        
        Counter += Time.deltaTime;
        if(Counter>= Random.Range(1f,10f)){
            GameObject g = Instantiate(Bullet,PlaceToShoot.transform.position,PlaceToShoot.transform.rotation,null);
            Instantiate(FXDisparo,PlaceToShoot.transform.position,transform.rotation);
            
            g.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-400f,-1000f),0,0));
            
            Counter = 0;
        }

    }

    
}
