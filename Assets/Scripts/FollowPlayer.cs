﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform Target;
    private float LimitX,LimitZ;
    public float MinX,MaxX;
    public float MinZ,MaxZ;

    // Update is called once per frame
    void Update()
    {
       // transform.rotation = Quaternion.Euler(64f,0,0);

        //LimitX = Mathf.Clamp(Target.transform.position.x,MinX,MaxX);
        LimitZ = Mathf.Clamp(Target.transform.position.z,MinZ,MaxZ); //Limites camara en X
        transform.position = new Vector3(transform.position.x,transform.position.y,LimitZ - 7);


       
    }
}
