﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rbd;
    public float speed;
    public int Score = 0;
    public ScoreManager scoreManager;
    public Animator anim;
    private Vector3 Direccion;
    public Image image;
    public float Vida = 1;
    private float maxVida = 100f;
    public float VPS = 10f;
    public float tiempoConVida = 0;
    public TextMeshProUGUI LifeTime;
    public CharacterController character;
    private float verticalVelocity;
    private float gravity = 14f;
    public bool run;
    


    
    void Start(){
        Vida = maxVida;
        image.fillAmount = maxVida;
        image.fillAmount = image.fillAmount * 100;;
        

    }
   
    void Update(){
       
       // Debug.Log(image.fillAmount);
        image.fillAmount -= Time.deltaTime / VPS;
        if(Input.GetKeyDown(KeyCode.X)){
            anim.SetTrigger("Attack");

        }
       
        
        
       

        Vida = image.fillAmount;
    }
    
    void FixedUpdate(){
        ScoreManager();
        Movement();
        LifeManager();
    }
     public void Movement(){
        
        
      anim.SetBool("run",run);
        

        Vector3 Direccion = Vector3.zero;
        //speed += Time.deltaTime * 8;
        if(Input.GetKey(KeyCode.W)){
            Direccion += Vector3.left;
            transform.rotation = Quaternion.Euler(0,-90,0);
            run = true;
        }else if(Input.GetKeyUp(KeyCode.W)){
            run = false;
        }
        if(Input.GetKey(KeyCode.S)){
            Direccion -= Vector3.left;
            transform.rotation = Quaternion.Euler(0,90,0);
             run = true;
        }else if(Input.GetKeyUp(KeyCode.S)){
            run = false;
        }
        if(Input.GetKey(KeyCode.A)){
            Direccion -= Vector3.forward;    
            transform.rotation = Quaternion.Euler(0,-180,0);      
              run = true;
        }else if(Input.GetKeyUp(KeyCode.A)){
            run = false;
        }
        if(Input.GetKey(KeyCode.D)){
            Direccion += Vector3.forward;
            transform.rotation = Quaternion.Euler(0,0,0);   
            run = true;
        }else if(Input.GetKeyUp(KeyCode.D)){
            run = false;
        }


        //Movimiento Diagonal
        

        verticalVelocity = -gravity * Time.deltaTime; //Gravedad
        Vector3 Gravedad = new Vector3(0,verticalVelocity,0);
        character.Move(Direccion * speed * Time.deltaTime + Gravedad);

         
        
    }
    public void LifeManager(){
       if(Vida<=0){
           gameObject.SetActive(false);
       }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Bullet"){
           Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Punto"){
            Debug.Log("Colision");
            Score = Score + 5;
            image.fillAmount += 0.3f;
            scoreManager.ManagerScore(Score);
            Destroy(other.gameObject);
        }
    }

    void ScoreManager(){
        tiempoConVida += Time.deltaTime;
        LifeTime.text = tiempoConVida.ToString("0.0");
    }

   
    
}
