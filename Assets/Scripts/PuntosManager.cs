﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntosManager : MonoBehaviour
{
    public Transform [] Puntos;
    
    public GameObject Punto;
    private float TimeToSpawn;
    private float counter;
    public float MinX,MaxX,MinZ,MaxZ;
    
    
    void Update()
    {
        float randX = Random.Range(MinX,MaxZ);
        float randZ = Random.Range(MinZ,MaxZ);

        TimeToSpawn = Random.Range(1f,10f);
        
        counter += Time.deltaTime;
        if(counter>TimeToSpawn){
            Instantiate(Punto,Puntos[Random.Range(0,Puntos.Length)].position = new Vector3(randX,transform.position.y,randZ),Quaternion.identity);
            counter = 0;
        }
    }
}
