﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public int Score;

    // Update is called once per frame
    public void ManagerScore(int Value){

        scoreText.text = "Score: " + Value.ToString("0");
    }
}
